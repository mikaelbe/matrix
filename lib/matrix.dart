library matrix;

import 'dart:math' as math;
import 'dart:mirrors';

part 'matrix/matrix.dart';
part 'matrix/matrixelement.dart';
part 'matrix/errors.dart';
part 'matrix/iterators.dart';
part 'matrix/double.dart';