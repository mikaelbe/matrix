part of matrix;

/**
 * The [_MatrixIterator] iterates over all elements in a [Matrix] in row-major
 * order.
 */
class _MatrixIterator<T> implements Iterator<T> {
  final Matrix _matrix;
  final int _end;
  int _position;
  T _current;
  
  _MatrixIterator(Matrix matrix) :
    _matrix = matrix,
    _end = matrix.length,
    _position = 0;
  
  T get current => _current;
  
  bool moveNext() {
    if (_position == _end) {
      _current = null;
      return false;
    }
    _current = _matrix._elements[_position];
    _position++;
    return true;
  }
}

/**
 * The [_MatrixRowIterator] iterates over the elements in some row of a
 * [Matrix].
 */
class _MatrixRowIterator<T> implements Iterator<T> {
  final Matrix _matrix;
  final int _end;
  final int _row;
  int _position;
  T _current;
  
  _MatrixRowIterator(Matrix matrix, int row) :
    _matrix = matrix,
    _row = row,
    _end = matrix._cols,
    _position = 0;
  
  T get current => _current;
  
  // Allows us to iterate by 'A.row(2)'
  _MatrixRowIterator<T> get iterator => this;
  
  bool moveNext() {
    if (_position == _end) {
      _current = null;
      return false;
    }
    _current = _matrix._elements[_row * _matrix._rows + _position];
    _position++;
    return true;
  }
}

/**
 * The [_MatrixColIterator] iterates over the elements in some column of a
 * [Matrix].
 */
class _MatrixColIterator<T> implements Iterator<T> {
  final Matrix _matrix;
  final int _end;
  final int _col;
  int _position;
  T _current;
  
  _MatrixColIterator(Matrix matrix, int col) :
    _matrix = matrix,
    _col = col,
    _end = matrix._rows,
    _position = 0;
  
  T get current => _current;
  
  // Allows us to iterate by 'A.col(2)'
  _MatrixColIterator<T> get iterator => this;
  
  bool moveNext() {
    if (_position == _end) {
      _current = null;
      return false;
    }
    _current = _matrix._elements[_position * _matrix._rows + _col];
    _position++;
    return true;
  }
}


