part of matrix;

class DimensionError extends Error {
  Matrix matrix;
  int row, col;
  
  DimensionError(this.matrix, this.row, this.col) : super();
  
  String toString() {
    if (matrix != null) {
      return 'DimensionError: matrix is (${matrix.rows},'
          '${matrix.cols}) (requested ($row,$col)).';
    }
    return 'DimensionError: requested ($row,$col).';
  }
      
}

class DimensionMismatchError extends Error {
  Matrix a;
  Matrix b;
  
  DimensionMismatchError(Matrix a, Matrix b);
  
  String toString() {
    return 'DimensionMismatchError: (${a.rows},${a.cols}) and '
        '(${b.rows},${b.cols})';
  }
}