part of matrix;

/**
 * MatrixElement defines a interface which all elements in a [Matrix] should
 * implement, i.e. you cannot create a Matrix of a type which does not
 * implement MatrixElement.
 */
abstract class MatrixElement {
  dynamic operator + (dynamic other);
  dynamic operator - (dynamic other);
  dynamic operator * (dynamic other);
  dynamic operator / (dynamic other);
  dynamic operator == (dynamic other);
}