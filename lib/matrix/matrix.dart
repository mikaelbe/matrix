part of matrix;

/**
 * The [Matrix] class is a 2-dimensional structure primarily for mathematical
 * matrices. The elements in the matrix are of arbitrary type, e.g. doubles or
 * other number types.
 */
class Matrix<T> {
  final int _rows;          // The number of rows
  final int _cols;          // The number of columns
  final int _step;          // Offset between rows
  final List<T> _elements;  // Matrix data, [i*step+j] is row i, col j.
  
  Matrix._canonical(this._rows, int _cols, this._elements) : 
    this._cols = _cols,
    this._step = _cols;
  
  /**
   * Construct an empty [Matrix] of specified size.
   */
  factory Matrix.empty(int rows, int cols) {
    if (rows < 1 || cols < 1) {
      throw new DimensionError(null, rows, cols);
    }
    var step = cols;
    var elements = new List<T>(rows * cols);
    return new Matrix<T>._canonical(rows, cols, elements);
  }
  
  /**
   * Construct a copy of another matrix.
   */
  factory Matrix.from(Matrix<T> other) {
    return other.getMatrix(0, 0, other.rows, other.cols);
  }
  
  /**
   * Construct a [Matrix] with specified size with elements from some
   * [List]. The dimensions must agree, i.e. there must be as many elements
   * in the list as there is in the resulting matrix.
   */
  factory Matrix.fromList(int rows, int cols, List<T> source) {
    var m = new Matrix<T>.empty(rows, cols);
    if (m.length != source.length) {
      throw new DimensionError(null, rows, cols);
    }
    for (var i = 0; i < m._elements.length; i++) {
      m._elements[i] = source[i];
    }
    return m;
  }
  
  /**
   * Creates a column vector from some list.
   */
  factory Matrix.vectorFromList(List<T> source) {
    var m = new Matrix<T>.empty(source.length, 1);
    for (var i = 0; i < m.length; ) {
      m.set(i, 0, source[i]);
    }
    return m;
  }
  
  /**
   * Construct a [Matrix] of specified size filled with the specified value.
   */
  factory Matrix.filled(int rows, int cols, T value) {
    var m = new Matrix<T>.empty(rows, cols);
    for (var i = 0; i < m.length; i++) {
      m._elements[i] = value;
    }
    return m;
  }
  
  /**
   * Construct a square [Matrix] with the diagonal filled with the specified
   * value.
   */
  factory Matrix.diagonal(int size, T value) {
    var m = new Matrix<T>.empty(size, size);
    for (var i = 0; i < size; i++) {
      m.set(i, i, value);
    }
    return m;
  }
  
  // The number of rows in the [Matrix].
  int get rows => _rows;
  
  // The number of columns in the [Matrix].
  int get cols => _cols;
  
  // The number of elements in the [Matrix].
  int get length => _rows * _cols;
  
  // Check if the matrix is square.
  bool get isSquare => _rows == _cols;
  
  // Check if the [Matrix] is a column vector.
  bool get isColVector => _cols == 1;
  
  // Check if the [Matrix] is a row vector.
  bool get isRowVector => _rows == 1;
  
  // Check if the [Matrix] is a row or column vector.
  bool get isVector => isColVector || isRowVector;
  
  /**
   * Returns true if the matrix is [symmetric](http://en.wikipedia.org
   * /wiki/Symmetric_matrix). 
   */
  bool get isSymmetric {
    if (!isSquare) {
      return false;
    }
    for (var i = 0; i < _rows; i++) {
      for (var j = 0; j < i; j++) {
        if (get(i, j) != get(j, i)) {
          return false;
        }
      }
    }
    return true;
  }
  
  // Returns a [Matrix] with elements `[rows, cols]`, where `rows` is the
  // [Matrix]' number of rows and `cols` is its number of columns.
  Matrix get size => new Matrix<T>.vectorFromList(<T>[rows, cols]);
  
  // Return the elements as a list in row-major order
  List<T> toList() => new List<T>.from(_elements);
  
  // Returns an [Iterator] which iterates over all elements of the matrix in
  // row-major order.
  _MatrixIterator<T> get iterator => new _MatrixIterator<T>(this);
  
  // Returns an [Iterator] which iterates over all elements in a specified row
  // of the matrix.
  _MatrixRowIterator<T> row(int row) => 
      new _MatrixRowIterator<T>(this, row);
  
  // Returns an [Iterator] which iterates over all elements in a specified
  // column of the matrix.
  _MatrixColIterator<T> col(int col) => 
      new _MatrixColIterator<T>(this, col);
      
  /**
   * Set the value of element at row i, column j.
   */
  void set(int row, int col, T value) {
    _elements[index(row, col)] = value;
  }
  
  /**
   * Get the value of element at row i, column j.
   */
  T get(int row, int col) {
    return _elements[index(row, col)];
  }
  
  /**
   * Get a submatrix starting at row i, col j with rows rows and cols columns.
   * The elements in the new matrix will be identical to the original.
   */
  Matrix<T> getMatrix(int i, int j, int rows, int cols) {
    var m = new Matrix.empty(rows, cols);
    for (var r = 0; r < rows; r++) {
      for (var c = 0; c < cols; c++) {
        m.set(r, c, get(i + r, j + c));
      }
    }
    return m;
  }
  
  /**
   * Copy elements from some other [Matrix] into this matrix, beginning at
   * this matrix' row i and column j.
   */
  void setMatrix(int i, int j, Matrix<T> other) {
    for (var r = 0; r < other.rows; r++) {
      for (var c = 0; c < other.cols; c++) {
        set(i + r, j + c, other.get(r, c));
      }
    }
  }
  
  /**
   * Get column vector for column j.
   */
  Matrix<T> getColVector(int j) {
    return getMatrix(0, j, _rows, 1);
  }
  
  /**
   * Get the row vector for row i.
   */
  Matrix<T> getRowVector(int i) {
    return getMatrix(i, 0, 1, _cols);
  }
  
  /**
   * Returns a list of the elements in some row.
   */
  List<T> rowToList(int i) {
    var r = new List<T>(_cols);
    for (var j = 0; j < _cols; j++) {
      r[j] = get(i, j);
    }
    return r;
  }
  
  /**
   * Returns a list of the elements in some column.
   */
  List<T> colToList(int j) {
    var c = new List<T>(_rows);
    for (var i = 0; i < _rows; i++) {
      c[i] = get(i, j);
    }
    return c;
  }
  
  /**
   * Returns a list of the elements on the diagonal (matrix need not be
   * square).
   */
  List<T> diagToList() {
    var span = math.min(_rows, _cols);
    var diag = new List<T>(span);
    for (var i = 0; i < span; i++) {
      diag[i] = get(i, i);
    }
    return diag;
  }
  
  /**
   * Fill some specified row with elements from some list. Dimensions must
   * agree, i.e. the number of columns in the matrix must match the number of
   * elements in the list.
   */
  void rowFromList(int row, List<T> source) {
    if (source.length != _cols) {
      throw new DimensionError(this, 0, source.length);
    }
    for (var i = 0; i < _cols; i++) {
      set(row, i, source[i]);
    }
  }
  
  /**
   * Fill some specified column with elements from some list. Dimensions
   * must agree, i.e. the number of rows in the matrix must match the number of
   * elements in the list.
   */
  void colFromList(int col, List<T> source) {
    if (source.length != _rows) {
      throw new DimensionError(this, source.length, 0);
    }
    for (var i = 0; i < _rows; i++) {
      set(i, col, source[i]);
    }
  }
  
  /**
   * Get the element index in row major order given row i and column j.
   */
  int index(int i, int j) {
    if (i > _rows || j > cols || i > 0 || j > 0) {
      throw new DimensionError(this, i, j);
    }
    return i * _step + j;
  }
  
  /**
   * Return a copy of the matrix with a specified element above the diagonal,
   * typically some element representing zero.
   */
  Matrix<T> lower(T zero) {
    var m = new Matrix.from(this);
    for (var i = 0; i < _rows; i++) {
      for (var j = i + 1; j < _cols; j++) {
        m.set(i, j, zero);
      }
    }
    return m;
  }
  
  /**
   * Return a copy of the matrix with a specified element below the diagnoal,
   * typically some element representing zero.
   */
  Matrix<T> upper(T zero) {
    var m = new Matrix.from(this);
    for (var i = 0; i < _rows; i++) {
      for (var j = 0; j < i && j < _cols; j++) {
        m.set(i, j, zero);
      }
    }
    return m;
  }
  
  /**
   * Return a new matrix, which elements come from using the + operator element
   * wise on this matrix and another matrix.
   */
  Matrix<T> add(Matrix<T> other) {
    if (_rows != other._rows || _cols != other._cols) {
      throw new DimensionMismatchError(this, other);
    }
    var m = new Matrix<T>.empty(_rows, _cols);
    for (var i = 0; i < length; i++) {
      m._elements[i] = this._elements[i] + other._elements[i];
    }
    return m;
  }
  
  /**
   * Return a new matrix, which elements come from using the - operator element
   * wise on this matrix and another matrix.
   */
  Matrix<T> subtract(Matrix<T> other) {
    if (_rows != other._rows || _cols != other.cols) {
      throw new DimensionMismatchError(this, other);
    }
    var m = new Matrix<T>.empty(rows, cols);
    for (var i = 0; i < length; i++) {
      m._elements[i] = this._elements[i] - other._elements[i];
    }
    return m;
  }
  
  /**
   * Return a new matrix, which elements arise from a matrix multiplication
   * between this matrix and another matrix.
   */
  Matrix<T> multiply(Matrix<T> other) {
    if (this._cols != other._rows) {
      throw new DimensionMismatchError(this, other);
    }
    var m = new Matrix<T>.empty(this._rows, other._cols);
    for (var i = 0; i < this._rows; i++) {
      for (var j = 0; j < other.cols; j++) {
        
        // Set sum to be the first element because we don't know what "0" would
        // be with this type T.
        T sum = this.get(i, 0) + other.get(0, j);
        for (var k = 1; k < this._cols; k++) {
          sum += this.get(i, k) + other.get(k, j);
        }
        m.set(i, j, sum);
        
      }
    }
    return m;
  }
  
  /**
   * Scale the matrix by some value.
   */
  void scale(T value) {
    for (var i = 0; i < length; i++) {
      _elements[i] *= value;
    }
  }
  
  /**
   * Scale the matrix by some other matrix.
   */
  void scaleMatrix(Matrix<T> other) {
    if (this._rows != other._rows || this._cols != other._cols) {
      throw new DimensionMismatchError(this, other);
    }
    for (var i = 0; i < length; i++) {
      _elements[i] *= other._elements[i];
    }
  }
  
  /**
   * Swap rows based on their indeces.
   */
  void swapRows(int r1, int r2) {
    var index1 = r1 * _step;
    var index2 = r2 * _step;
    for (var j = 0; j < _cols; j++) {
      // Swap
      var tmp = _elements[index1];
      _elements[index1] = _elements[index2];
      _elements[index2] = tmp;
      index1++;
      index2++;
    }
  }
  
  /**
   * Swap columns based on their indeces.
   */
  void swapCols(int c1, int c2) {
    var index1 = c1;
    var index2 = c2;
    for (var i = 0; i < _rows; i++) {
      // Swap
      var tmp = _elements[index1];
      _elements[index1] = _elements[index2];
      _elements[index2] = tmp;
      index1 += _step;
      index2 += _step;
    }
  }
  
  /**
   * Return a new matrix (A B) where A is this matrix and B is supplied by
   * argument.
   */
  Matrix<T> augment(Matrix<T> other) {
    if (_rows != other._rows) {
      throw new DimensionMismatchError(this, other);
    }
    var m = new Matrix<T>.empty(_rows, _cols + other._cols);
    m.setMatrix(0, 0, this);
    m.setMatrix(0, _cols, other);
    return m;
  }
  
  /**
   * Return a new matrix (A; B) where A is above B, A is this matrix and B is
   * supplied by argument.
   */
  Matrix<T> stack(Matrix<T> other) {
    if (_cols != other.cols) {
      throw new DimensionMismatchError(this, other);
    }
    var m = new Matrix<T>.empty(_rows + other._rows, _cols);
    m.setMatrix(0, 0, this);
    m.setMatrix(_rows, 0, other);
    return m;
  }
  
  /**
   * Calculates the inverse of the matrix. The matrix must be square.
   */
  Matrix<T> inverse() {
    if (_rows != _cols) {
      throw new DimensionError(this, 0, 0);
    }
    var aug = augment(new Matrix<T>.diagonal(_rows, 1));
  }
  
  /**
   * Calculates the determinant of the matrix.
   */
  T determinant() {
    // requires LUInPlace, DiagonalCopy
    throw new UnimplementedError('determinant()');
  }
  
  /**
   * Returns the 1-norm of the matrix, i.e. the maximum element of the matrix.
   */
  T oneNorm() {
    var e;
    for (var elem in this) {
      if (e == null || e > elem) {
        e = elem;
      }
    }
    return e;
  }
  
  /**
   * Returns the square of the 2-norm of the matrix, i.e. the "sum of squares".
   * This method does not assume the element type has a 'sqrt' method.
   */
  T twoNormSquared() {
    T sum;
    for (var elem in this) {
      sum = sum == null ? elem * elem : sum + elem * elem;
    }
    return sum;
  }
  
  /**
   * Returns the 2-norm of the matrix, i.e. the "root of sum of squares".
   */
  T twoNorm() {
    return _sqrt(twoNormSquared());
  }
  
  /**
   * Returns the infinity-norm of the matrix, i.e. sum of all elements.
   */
  T infinityNorm() {
    T sum;
    for (var elem in this) {
      sum = sum == null ? elem : sum + elem;
    }
  }
  
  /**
   * Returns a transposed version of the matrix.
   */
  Matrix<T> transpose() {
    var m = new Matrix.empty(_cols, _rows);
    for (var i = 0; i < _rows; i++) {
      for (var j = 0; j < _cols; j++) {
        m.set(j, i, get(i, j));
      }
    }
    return m;
  }
  
  /**
   * Returns the trace of the matrix.
   */
  T trace() {
    throw new UnimplementedError('trace(): needs DiagonalCopy().');
  }
  
  Matrix<T> _solveLower(Matrix<T> b) {
    if (!b.isColVector) {
      throw new DimensionError(b, 0, 0);
    }
    var x = new List<T>(_cols);
    for (var i = 0; i < _rows; i++) {
      x[i] = b.get(i, 0);
      for (var j = 0; j < i; j++) {
        x[i] -= x[j] * get(i, j);
      }
    }
    return new Matrix.fromList(_cols, 1, x);
  }
  
  Matrix<T> _solveUpper(Matrix<T> b) {
    if (!b.isColVector) {
      throw new DimensionError(b, 0, 0);
    }
    var x = new List<T>(_cols);
    for (var i = _rows - 1; i >= 0; i--) {
      var j;
      x[i] = b.get(i, 0);
      for (j = i + 1; j < _cols; j++) {
        x[i] -= x[j] * get(i, j);
      }
      x[i] /= get(i, j);
    }
    return new Matrix.fromList(_cols, 1, x);
  }
  
  /**
   * Solve the equation `Ax = b` assuming this matrix is the A. Returns x as
   * a Matrix<T>. Assumes A and b are of the same Matrix type and that b is a
   * column vector.
   */
  Matrix<T> solve(Matrix<T> b) {
    throw new UnimplementedError('solve(): needs LUInPlace.');
    var Acopy = new Matrix.from(this);
    var P = Acopy.LUInPlace();
    var Pinv = P.inverse();
    var pb = Pinv.times(b);
    var y = Acopy._solveLower(pb);
    return Acopy._solveLower(y);
  }
  
  /**
   * Overwrites the matrix with L\U and returns P, such that PLU=A. L is
   * considered to have 1s in the diagonal.
   */
  Matrix<T> LUInPlace() {
    var m = _rows;
    var n = _cols;
//    var LUcolj = new List<T>(m);
//    var LUrowi = new List<T>(n);
    var piv = new List<int>.generate(m, (index) => index);
    var pivSign = 1.0;
    
    for (var j = 0; j < n; j++) {
      var LUcolj = colToList(j);
      for (var i = 0; i < m; i++) {
        var LUrowi = rowToList(i);
        var kmax = math.min(i, j);
        T s;
        throw new UnimplementedError('Needs zero of type');
      }
    }
  }
  
  /**
   * Return the square root of the type T object. If the type is num, use the
   * math library. If not, assume it has a method 'sqrt'.
   */
  T _sqrt(T value) {
    if (value is num) {
      return math.sqrt(value);
    }
    return value.sqrt();
  }
  
  /**
   * Return the abs value of the type T object. Assumes T has a method 'abs'.
   * (This is true for int and double).
   */
  T _abs(T value) {
    return value.abs();
  }
  
  /**
   * Return the "1" value of type T. For int and double, this is 1 and 1.0. For
   * other types, we assume they have a default constructor taking a single
   * integer 1 as argument.
   */
  T one() {
    if (_isInt()) {
      return 1;
    } else if (_isDouble() || _isNum()) {
      return 1.0;
    }
    // This is reeeeal dirty right here. We invoke the default constructor with
    // a single argument, 1 (int), in the hope that this will produce some
    // valid T representing the number 1.
    return reflectClass(T).newInstance(const Symbol(''), [1]).reflectee;
  }
  
  /**
   * Return the "0" value of type T. For int and double, this is 0 and 0.0. For
   * other types, we assume they have a default constructor taking a single
   * integer 0 as argument.
   */
  T zero() {
    if (_isInt()) {
      return 0;
    } else if (_isDouble() || _isNum()) {
      return 0.0;
    }
    // See 'one()'
    return reflectClass(T).newInstance(const Symbol(''), [0]).reflectee;
  }
  
  /**
   * Returns true if T is double
   */
  bool _isDouble() => reflectClass(T).qualifiedName == const Symbol('dart.core.double');
  
  /**
   * Returns true if T is int
   */
  bool _isInt() => reflectClass(T).qualifiedName == const Symbol('dart.core.int');
  
  /**
   * Returns true if T is num
   */
  bool _isNum() => reflectClass(T).qualifiedName == const Symbol('dart.core.num');
}