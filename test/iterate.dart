part of matrix_test;

void test_iterate() {

  group('matrix iterators', () {
    
    List<int> elements;
    Matrix A;
    
    setUp(() {
      elements = new List<int>.generate(16, (int index) => index);
      A = new Matrix<int>.fromList(4, 4, elements);
    });
    
    test('iterate over all elements in a matrix in row-major order', () {
      var i = 0;
      for (var elem in A) {
        expect(elem, equals(i));
        i++;
      }
    });
    
    test('iterate over all elements in some row', () {
      var i = 0;
      for (var elem in A.row(0)) {
        expect(elem, equals(i));
        i++;
      }
    });
    
    test('iterate over all elements in some column', () {
      var i = 0;
      for (var elem in A.col(0)) {
        expect(elem, equals(i));
        i = i + A.cols;
      }
    });
    
//    test('REMOVE: test the one method', () {
//      var B = new Matrix<double>.filled(2, 2, 0.0);
//      expect(B.one(), equals(1.0)); 
//    });
    
    test('double-matrix', () {
      var D = new Matrix<double>.filled(4, 4, 0.0);
      for (var elem in D) {
        expect(elem, equals(0.0));
      }
      expect(D.one(), equals(1.0));
    });
    
  });
}