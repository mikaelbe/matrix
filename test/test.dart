library matrix_test;

import 'package:unittest/unittest.dart';
import 'package:rational/rational.dart';
import '../lib/matrix.dart';

part 'iterate.dart';
part 'rational.dart';

void main() {
  test_iterate();
  test_rational();
}