import 'package:unittest/unittest.dart';
import 'package:unittest/html_config.dart';
import 'test.dart' as t;

main() {
  useHtmlConfiguration();
  t.main();
}