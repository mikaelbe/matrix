part of matrix_test;

class _IsRational extends TypeMatcher {
  const _IsRational () : super('Rational');
  bool matches(item, Map matchState) => item is Rational;
}
const isRational = const _IsRational();

void test_rational() {

  group('Rational matrices', () {
    
    Matrix<Rational> A = new Matrix<Rational>.filled(4, 4, new Rational(1));
    
    test('check that all elements are Rational', () {
      var i = 0;
      for (var elem in A) {
        expect(elem, isRational);
      }
    });
    
    test('check that the one() produces a valid Rational', () {
      var one = A.one();
      expect(one, equals(new Rational(1)));
    });
    
  });
}