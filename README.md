Matrix
======

This is a matrix library for Dart. The library provides a generic data
structure generic where the elements typically are doubles, but can be of any
object which implement the arithmetic operators.

Note
----

This project is currently in very early development, and many planned
operations are not yet implemented.